// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA9DzMsyLlA10DhYLS4fRxmI3NT8Gkfvjw",
    authDomain: "final-brian.firebaseapp.com",
    databaseURL: "https://final-brian.firebaseio.com",
    projectId: "final-brian",
    storageBucket: "final-brian.appspot.com",
    messagingSenderId: "209692216611",
    appId: "1:209692216611:web:5f6f8a41b85393560c5096",
    measurementId: "G-V265CEF5SC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
