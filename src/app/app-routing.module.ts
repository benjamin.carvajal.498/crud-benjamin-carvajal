import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'lista-persona', loadChildren: './lista-persona/lista-persona.module#ListaPersonaPageModule' },
  { path: 'modal-persona', loadChildren: './modal-persona/modal-persona.module#ModalPersonaPageModule' },
  { path: 'detalles/:id', loadChildren: './detalles/detalles.module#DetallesPageModule' },
  { path: 'detalles', loadChildren: './detalles/detalles.module#DetallesPageModule' },
  { path: 'crear', loadChildren: './crear/crear.module#CrearPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
