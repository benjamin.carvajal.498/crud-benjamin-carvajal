import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPersonaPage } from './lista-persona.page';

describe('ListaPersonaPage', () => {
  let component: ListaPersonaPage;
  let fixture: ComponentFixture<ListaPersonaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPersonaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPersonaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
