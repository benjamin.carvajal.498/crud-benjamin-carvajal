import { Component, OnInit } from '@angular/core';
import { TaskI } from '../model/task.interface';
import { TodoService } from '../services/todo.service';
@Component({
  selector: 'app-lista-persona',
  templateUrl: './lista-persona.page.html',
  styleUrls: ['./lista-persona.page.scss'],
})
export class ListaPersonaPage implements OnInit {

  constructor(private personaServicio: TodoService) { }
  public personas: TaskI[];
  ngOnInit() {
    this.personaServicio.obtenerPersonas().subscribe(res =>{
      this.personas = res
    });
  }

  eliminar(id){
    this.personaServicio.eliminarPersona(id);
  }
  
}
