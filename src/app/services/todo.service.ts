import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TaskI} from '../model/task.interface';
import { THIS_EXPR } from '../../../node_modules/@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private PersonasCollection: AngularFirestoreCollection<TaskI>;
  private Persona: Observable<TaskI[]>;
  constructor(db:AngularFirestore) {
    this.PersonasCollection = db.collection<TaskI>("personas");
    this.Persona = this.PersonasCollection.snapshotChanges().pipe(map( 
      actions =>{
        return actions.map( a =>{
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id,...data }
        });
       }

    )); 
   }

   obtenerPersonas(){
     return this.Persona;
   }

   obtenerPersona(id:string){
     return this.PersonasCollection.doc<TaskI>(id).valueChanges();
   }

   actualizarPersona(persona: TaskI, id: string){
     return this.PersonasCollection.doc(id).update(persona);
   }

   añadirPersona(persona: TaskI){
     return this.PersonasCollection.add(persona);
   }

   eliminarPersona(id:string){
     return  this.PersonasCollection.doc(id).delete();
   }
}
