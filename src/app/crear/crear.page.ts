import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';
import { TaskI } from '../model/task.interface';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.page.html',
  styleUrls: ['./crear.page.scss'],
})
export class CrearPage implements OnInit {
   persona: TaskI = {
    nombre: '',
    apellido: ''   
  };
  constructor(private personaServicio: TodoService) { }

  ngOnInit() {


  }

  guardar(){
    this.personaServicio.añadirPersona(this.persona);
  }
}
