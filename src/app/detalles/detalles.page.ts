import { Component, OnInit } from '@angular/core';
import { TaskI } from '../model/task.interface';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { NavController, LoadingController } from '../../../node_modules/@ionic/angular';
import { TodoService } from '../services/todo.service';


@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage implements OnInit {

  persona: TaskI = {
    nombre: '',
    apellido: ''
  };

  personaId= null;

  constructor(private route: ActivatedRoute, private nav: NavController, private personaService: TodoService, private loadingController: LoadingController) { }

  ngOnInit() {
    debugger;
    this.personaId = this.route.snapshot.params['id'];
    this.loadPersona();
  }

   loadPersona(){
    this.personaService.obtenerPersona(this.personaId).subscribe(Persona => {
      this.persona = Persona;
    });
  }

  guardar(){
    this.personaService.actualizarPersona(this.persona,this.personaId);
  }


}
